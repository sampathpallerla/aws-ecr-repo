variable "region" {
    type = string
    default = "ap-southeast-2"
}

variable "dynamo" {
    type = string
    default = "terraform-lock"
}

variable "shs_account" {
  type        = string
  description = "the shared service account id that will manage the "
  default = "537959585930"
  
}

variable "build_accounts" {
  type        = list(string)
  description = "the accounts that will need read and write access to image content"
  default = ["537959585930"]
}

variable "runtime_accounts" {
  type        = list(string)
  description = "the accounts that will only need read access to run the container images"
  default = ["537959585930"]
}


