terraform {
  backend "s3" {
    bucket         = "sampath00007"
    key            = "terraform/tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "terraform-lock"
    role_arn       = "arn:aws:iam::537959585930:role/ops"
    
  }
}


resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = var.dynamo
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20
 
  attribute {
    name = "LockID"
    type = "S"
  }
 
}
