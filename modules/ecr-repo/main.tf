resource "aws_ecr_repository" "this" {
 name = "${var.domain}/${var.name}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository_policy" "this" {
  repository = aws_ecr_repository.this.name
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          # read repository content permissions assigned to builder and runtime accounts (non-prod and prod)
          "Effect" : "Allow",
          "Action" : [
            "ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:BatchGetImage",
            "ecr:GetDownloadUrlForLayer"
          ],
          "Principal" : { 
            "AWS" : ["arn:aws:iam::537959585930:root"]
          
          },
        },
        {
          # manage repository content (images) are only assigned to builder accounts (non-prod)
          "Effect" : "Allow",
          "Action" : [
            "ecr:ListImages",
            "ecr:DescribeImages",
            "ecr:PutImage",
            "ecr:UploadLayerPart",
            "ecr:InitiateLayerUpload",
            "ecr:CompleteLayerUpload",
            "ecr:ListTagsForResource"
          ],
          "Principal" : {
            "AWS" : [for id in var.build_account_ids : "arn:aws:iam::${id}:root"]
          }
          }, {
          # administer repository and content (images) are only performed in the shared services account
          "Effect" : "Allow",
          "Action" : [
            "ecr:*"
          ],
          "Principal" : {
            "AWS" : [
              "arn:aws:iam::${var.shs_account_id}:root"
            ]
          }
        }
      ]
    }
  )
}

resource "aws_ecr_lifecycle_policy" "this" {
  repository = aws_ecr_repository.this.name
  policy = jsonencode(
    {
      "rules" : [
        {
          "rulePriority" : 1,
          "description" : "Expire untagged images older than 14 days",
          "selection" : {
            "tagStatus" : "untagged",
            "countType" : "sinceImagePushed",
            "countUnit" : "days",
            "countNumber" : 14
          },
          "action" : {
            "type" : "expire"
          }
        }
      ]
    }
  )
}
