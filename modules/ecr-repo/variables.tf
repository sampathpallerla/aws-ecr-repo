variable "domain" {
  type        = string
  description = "the business domain the container image belongs to"
}

variable "owner" {
  type        = string
  description = "the business owner of the container image"
}

variable "service_name" {
  type        = string
  description = "the name of the application the container belongs to"
}

variable "billing_code" {
  type        = string
  description = "the name of the service or application the container image belongs to"
}

variable "env" {
  type        = string
  description = "the name of the container image environment, defaults to core since container images are not normally bound to environments (see https://devops.vpages.co/handbook/standards/naming/)"
  default     = "core"
}

variable "name" {
  type        = string
  description = "the name of the container to create"
}

variable "shs_account_id" {
  type        = string
  description = "the shared service account id that will manage the "
  default = "118739801867"
}

variable "build_account_ids" {
  type        = list(string)
  description = "the accounts that will need read and write access to image content"
}

variable "runtime_account_ids" {
  type        = list(string)
  description = "the accounts that will only need read access to run the container images"
}


