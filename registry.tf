locals {
  # FIXME this configuration must be replaced with filtered data from the landing zone
  ec_runtime_account_ids = [
    "537959585930"
  ]
  ec_build_account_ids = [
    "537959585930"
  ]
  shs_account_id = "537959585930"
}

module "ec_refd_company_api" {
  source              = "./modules/ecr-repo"
  domain              = "ec"
  owner               = "devops"
  billing_code        = "80:8802"
  service_name        = "refd-platform"
  name                = "refd-company-api"
  shs_account_id      = local.shs_account_id
  runtime_account_ids = local.ec_runtime_account_ids
  build_account_ids   = local.ec_build_account_ids
}


module "ec_refd_search_airportsapi" {
  source              = "./modules/ecr-repo"
  domain              = "ec"
  owner               = "devops"
  billing_code        = "80:8802"
  service_name        = "refd-platform"
  name                = "refd-search-airport-api"
 shs_account_id      = local.shs_account_id
  runtime_account_ids = local.ec_runtime_account_ids
  build_account_ids   = local.ec_build_account_ids

}

module "ec_send_itinerary_api" {
  source              = "./modules/ecr-repo"
  domain              = "ec"
  owner               = "devops"
  billing_code        = "80:8803"
  service_name        = "trip-services"
  name                = "send-itinerary-api"
  shs_account_id      = local.shs_account_id
  runtime_account_ids = local.ec_runtime_account_ids
  build_account_ids   = local.ec_build_account_ids
  
  }
